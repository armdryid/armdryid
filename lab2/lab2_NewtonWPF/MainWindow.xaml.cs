﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab2_NewtonWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Задаем программе число, точность и степень корня
    /// С помомощью метода Ньютона программа вычисляет значение корня
    /// Далее выводит его и значение вычисленное с помощью math.Pow
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //getting value from textbox
            double chi = Convert.ToDouble(chiBox.Text);

            //getting value for eps from textbox
            double eps = Convert.ToDouble(epsBox.Text);

            //getting value for stepen from textbox
            double stepen = Convert.ToDouble(stepenBox.Text);


            double otvet = chi/stepen;
            double nx = (1/stepen)*((stepen-1)*otvet+chi/Math.Pow(otvet, stepen-1));

            while(Math.Abs(nx-otvet)>eps)
            {
                otvet = nx;
                nx = (1 / stepen) * ((stepen - 1) * otvet + chi / Math.Pow(otvet, stepen - 1));
            }
           


            ourAnswer.Text = otvet.ToString();
            powAnswer.Text = Math.Pow(chi, 1/stepen).ToString();
        }

    }
}
