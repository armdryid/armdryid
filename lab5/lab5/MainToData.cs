﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    public class MainToData
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }


        public  void  razdel (string s )
        {
            
            char delitel = ',';
            String[] substrings = s.Split(delitel);
            char[] Trimer = { ' ' };
            Name = substrings[0].Trim(Trimer);
            City = substrings[1].Trim(Trimer);
            State = substrings[2].Trim(Trimer);
            Zip = substrings[3].Trim(Trimer);
        }

        public string Vmeste(string s )
        {

            s = Name + " живет в " + City + State + " область " + Zip + " почтовый индекс ";
            return s;

        }
    }
}

