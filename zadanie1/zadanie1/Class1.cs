﻿using System;
using System.IO;

class Class1
{
    private string outPut;

    private static string l, y, x;

    public void outPutString()
    {
        Console.WriteLine(outPut);
    }

    public Class1(string path)
    {
        try
        {
            using (StreamReader reader = new StreamReader(path, System.Text.Encoding.Default))
            {
                while (!reader.EndOfStream)
                {
                    l = reader.ReadLine();

                    l = l.Replace(",", " ");
                    double y =Convert.ToDouble(l.Substring(l.IndexOf(" ")));
                    double x =Convert.ToDouble(l.Substring(0, l.IndexOf(" ")));
                    
                    outPut += "X: " + x + " Y:" + y + "\n";
                }

            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
           
        }
    }

}

